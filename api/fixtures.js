const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');


const User = require('./models/User');
const Place = require('./models/Place');




const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await  collection.drop()
    }

    const user = await User.create(
        {
            username: 'admin',
            password: '123',
            role: 'admin',
            token: nanoid(),
        },
        {
            username: 'Alex',
            password: '123',
            role: 'user',
            token: nanoid(),
        },
        {
            username: 'Ben',
            password: '123',
            role: 'user',
            token: nanoid(),
        },
    );


    const place = await Place.create(
        {
            user: user[1]._id,
            title: 'Аврора',
            mainImage: 'aurora.jpg',
            description: 'банкетный зал на 500 человек, 12 кухонь мира предоставят вашему вниманию одно из лучших блюд европейской, корейской и национальной классики'
        },
        {
            user: user[2]._id,
            title: 'Сеул',
            mainImage: 'seoull.jpeg',
            description: 'Южно-корейская кухня с изобилием чеснока, красного перца и специй. Редкий вкус острых и разнообразных блюд и салатов корейской кухни'
        }


    );

    return connection.close();

};

run().catch(error => {
    console.log("Something wrong happened...", error)
});