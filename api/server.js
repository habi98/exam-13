const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/users');
const places = require('./app/places');
const comments = require('./app/comments');


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

let port = 8000;


if (process.env.NODE_ENV === 'test') {
    port = 8010;
}


mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/places', places);
    app.use('/comments', comments);


    app.listen(port, () => {
        console.log(`Server started on ${port} port`)
    })
});

