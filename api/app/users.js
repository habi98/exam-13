const express = require('express');

const User = require('../models/User');


const router = express.Router();

router.post('/', async (req, res) => {
    try {
        console.log(req.body);

        const userData = req.body;


        const user = await User(userData);

        user.generateToken();

        await user.save();

        return res.send({message: 'User register', user})
    } catch (e) {
        return res.status(400).send(e)
    }
});


router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(401).send({error: 'Неверный логин или пароль'})
    }

    const isMath = await user.checkPassword(req.body.password);

    if (!isMath) {
        return res.status(401).send({error: 'Неверный логин или пароль'})
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Login successful', user})

});


router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};

    if(!token) {
        return res.send(success);
    }
    const user = await User.findOne({token});

    if(!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    return res.send(success);
});


module.exports = router;