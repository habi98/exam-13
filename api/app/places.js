const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');


const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const tryAuth = require('../middleware/tryAuth');

const Place = require('../models/Place');



const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});


const upload = multer({storage});
const router = express.Router();


router.get('/', tryAuth, async (req, res) => {
    try {
        const places = await Place.find();
        return res.send(places)
    } catch (e) {
        res.sendStatus(500)
    }
});

router.get('/:id', async (req, res) => {
    try {
        const place = await Place.findById(req.params.id);

        return res.send(place)
    } catch (e) {
        return res.sendStatus(500)
    }
});

router.post('/', auth, upload.single('mainImage'), async(req, res) => {

    try {
        const placeData = req.body;

        console.log(placeData.conditions);

        if (placeData.conditions === "false") {
            return res.status(400).send({message: "Вы не приняли условия"})
        }

        if (req.file) {
            placeData.mainImage = req.file.filename

        }


        placeData.user = req.user._id;

        const place = await Place(placeData);

        await place.save();

        return res.send(place);

    } catch (error) {
        return res.status(400).send(error)
    }
});

router.post('/:id', auth, upload.array('gallery'), async (req, res) => {

    const place = await Place.findById(req.params.id);

    if (req.files) {
        req.files.map(file => place.gallery.push(file.filename))

    }

    place.save()
        .then((result) => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth, permit('admin')],  (req, res) => {
    Place.findByIdAndDelete({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error=>res.status(403).send(error))

});


module.exports = router;