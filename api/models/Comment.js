const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
   placeId: {
        type: Schema.Types.ObjectId,
        ref: 'Place',
        required: true
    },
    commentText: {
        type: String,
        required: true
    },
    ratingQualityOfFood: {
        type: Number,
    },
    ratingServiceQuality: {
        type: Number
    },
    ratingInterior: {
        type: Number
    },
    dateTime: String,
});


const Comment = mongoose.model('Comment', CommentSchema);


module.exports = Comment;


