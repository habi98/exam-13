import React from 'react';
import config from "../../../config";

import Slider from 'react-slick';

import "./SlickCarousel.css";
import './CarouselGallery.css'


const CarouselGallery = ({place}) => {
    const settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1
    };
    console.log(place, 'place');

    return (
        <div className="gallery-slider">
            <Slider {...settings}>
                {
                    place.gallery.length > 0 &&  place.gallery.map(slider => (
                        <img key={slider} className='Slide-img' src={config.apiUrl + '/uploads/' + slider} alt={place.title}/>
                    ))
                }
            </Slider>
        </div>
    );
};

export default CarouselGallery;