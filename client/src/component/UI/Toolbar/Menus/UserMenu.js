import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";


const UserMenu = ({user, logout}) => {
    return (
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Hello!, {user.user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem>
                    <NavLink to="/new/place" tag={RouterNavLink}>Add new place</NavLink>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem onClick={logout}>
                    logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
    );
};

export default UserMenu;