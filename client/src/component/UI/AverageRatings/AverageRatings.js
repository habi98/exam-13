import React, {Fragment} from 'react';
import {Col, Row} from "reactstrap";
import StarRatings from 'react-star-ratings'

const AverageRatings = (props) => {
    return (
        <Fragment>

            <hr/>
            <h3>Ratings</h3>
            <Row>
                <Col sm={2}>
                    Overall
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numOverall}
                        ratingToShow={props.numOverall}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numOverall}
                </span>
                </Col>
                <Col sm={2}>
                    Quality Of Food
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numQualityOfFood}
                        ratingToShow={props.numQualityOfFood}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numQualityOfFood}
                </span>
                </Col>

                <Col sm={2}>
                    Service Quality
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numServiceQuality}
                        ratingToShow={props.numServiceQuality}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numServiceQuality}
                </span>
                </Col>
                <Col sm={2}>
                    Interior
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numInterior}
                        ratingToShow={props.numInterior}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numInterior}
                </span>
                </Col>
            </Row>
        </Fragment>
    );
};

export default AverageRatings;