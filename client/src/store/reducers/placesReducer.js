import {FETCH_ONE_PLACE_SUCCESS, FETCH_PLACES_SUCCESS} from "../actions/placesActions";

const initialState = {
    places: [],
    place: null
};

const placesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PLACES_SUCCESS:
            return {...state, places: action.places};
        case  FETCH_ONE_PLACE_SUCCESS:
            return {...state, place: action.place};
        default:
            return state
    }
};

export default placesReducer