import {push} from 'connected-react-router';
import axios from '../../axios-api'
import {NotificationManager} from "react-notifications";


export const FETCH_PLACES_SUCCESS = 'FETCH_PLACES_SUCCESS';
export const FETCH_ONE_PLACE_SUCCESS = 'FETCH_ONE_PLACE_SUCCESS';


export const fetchPlacesSuccess = (places) => ({type: FETCH_PLACES_SUCCESS, places});
export const fetchOnePlaceSuccess  = (place) => ({type: FETCH_ONE_PLACE_SUCCESS, place});

export const fetchPlaces = () => {
    return dispatch => {
        return axios.get('/places').then(
            response => {
                dispatch(fetchPlacesSuccess(response.data))
            }
        )
    }
};

export const fetchOnePlace = (placeId) => {
    return dispatch => {
        return axios.get('/places/' + placeId).then(
            response => {
                dispatch(fetchOnePlaceSuccess(response.data))
            }
        )
    }
};

export const createPlace = (placeData) => {

    return (dispatch)=> {
        return axios.post('/places', placeData).then(
            () => {
                dispatch(push('/'))
            },
            error => {
                if (error.response) {
                    console.log(error.response)
                    NotificationManager.error(error.response.data.message)
                }
            }
        )
    }
};


export const createPlaceGallery = (placeId, gallery) => {
    return dispatch => {
        return axios.post('/places/' + placeId, gallery).then(
            () => {
                dispatch(fetchOnePlace(placeId));
                NotificationManager.success('Вы успешно добавили изображение')
            }
        )
    }
};

export const deleteOnePlace = (placeId) => {
    return dispatch => {
        return axios.delete('/places/' + placeId).then(
            response => {
                dispatch(fetchPlaces());
                NotificationManager.success('Успешно удалено')
            }

        )
    }
}