import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications'

import axios from '../../axios-api'

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = "LOGIN_USER_FAILURE";
export const LOGOUT_USER = 'LOGOUT_USER';


export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});
export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});


export const logoutUser = () => {
    return (dispatch) => {
        return axios.delete('/users/sessions').then(
            () => {
                dispatch({type: LOGOUT_USER});
                NotificationManager.success('Вы вышли!');
                dispatch(push('/login'));
            },
            error => {
                NotificationManager.error('Не удалось выйти!');
            }
        )
    }
};

export const registerUser = userData => {
    return (dispatch) => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess(response.data));
                NotificationManager.success("Вы успешно зарегистрировались");
                dispatch(push('/'))
            },
            error => {
                if (error.response) {
                    dispatch(registerUserFailure(error.response.data))
                } else {
                    dispatch(registerUserFailure({global: "Нет соединения"}))
                }
            }
        )
    }
};


export const loginUser = (userData) => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data));
                NotificationManager.success('Вы успешно вошли в систему');
                dispatch(push('/'))
            },
            error => {
                if (error.response) {
                    dispatch(loginUserFailure(error.response.data))
                } else {
                    dispatch(loginUserFailure({global: 'Нкт соединения'}))
                }
            }
        )
    }
};





