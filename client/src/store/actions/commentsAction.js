import axios from '../../axios-api'
import {NotificationManager} from "react-notifications";

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';
export const fetchCommentsSuccess = (comments) => ({type: FETCH_COMMENTS_SUCCESS, comments});

export const fetchComments = (placeId) => {
    return dispatch => {
        let url = '/comments';

        if (placeId) {
            url += `?placeId=${placeId}`
        }
        return axios.get(url).then(
            response => {
                dispatch(fetchCommentsSuccess(response.data))
            }
        )
    }
};


export const createComment = (commentData) => {
    return dispatch => {
        return axios.post('/comments', commentData).then(
            () => {
                dispatch((fetchComments(commentData.placeId)));
                NotificationManager.success('Вы успешно добавили коммент')
            }
        )
    }
};