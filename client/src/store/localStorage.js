export const saveToLocalStorage = state => {
    try {
        const seralizedState = JSON.stringify(state)
        localStorage.setItem('state', seralizedState);
    } catch (e) {
        console.log('Could not save state')
    }
};

export const loadFromLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('state')
        if (serializedState === null) {
            return undefined
        }

        return JSON.parse(serializedState)
    } catch (e) {
        return undefined
    }
};

