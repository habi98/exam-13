import React from 'react';
import {Route, Switch} from 'react-router-dom'
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import Places from "./container/Places/Places";
import PlacePage from "./container/PlacePage/PlacePage";
import AddPlace from "./container/NewPlace/NewPlace";



const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={Places}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/place/:id" exact component={PlacePage}/>
            <Route path="/new/place" exact component={AddPlace}/>
        </Switch>
    );
};

export default Routes;