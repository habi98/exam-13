import React, {Component, Fragment} from 'react';
import Toolbar from "./component/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import Routes from "./Routes";
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/userActions";


class App extends Component{
    render() {
        return (
            <Fragment>
                <header>
                     <Toolbar user={this.props.user} logout={this.props.logoutUser}/>
                    <NotificationContainer/>
                </header>
                <Container>
                    <Routes/>
                </Container>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
