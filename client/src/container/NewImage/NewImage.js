import React, {Component} from 'react';
import {Button, Col, Form} from "reactstrap";
import FormElement from "../../component/UI/Form/FormElement";
import FormGroup from "reactstrap/es/FormGroup";
import {connect} from "react-redux";
import {createPlaceGallery} from "../../store/actions/placesActions";

class NewImage extends Component {
    state = {
        gallery: [],
    };


    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    gallery: [...this.state.gallery, file],
                });
            };
            reader.readAsDataURL(file);
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        console.log(formData, 'formData');

        for (let i = 0; i < this.state.gallery.length; i++) {
            formData.append('gallery', this.state.gallery[i]);
        }

        this.props.createPlaceGallery(this.props.placeId, formData);
    };

    render() {
        return (
            <Form className="pt-3">
                <hr/>
                <h3>Add a photo</h3>
                <FormElement
                    type="file"
                    propertyName="gallery"
                    title="Images"
                    id="gallery"
                    multiple
                    onChange={this.galleryChangeHandler}
                />
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary" onClick={this.submitFormHandler}>
                            Upload
                        </Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createPlaceGallery: (placeId, gallery) => dispatch(createPlaceGallery(placeId, gallery))
});

export default connect(null, mapDispatchToProps)(NewImage);