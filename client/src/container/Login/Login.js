import React, {Component} from 'react';
import Form from "reactstrap/es/Form";
import FormElement from "../../component/UI/Form/FormElement";
import {Alert, Button, Col, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {loginUser} from "../../store/actions/userActions";

class Login extends Component {
    state = {
        username: '',
        password: ''
    };



    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = (event) => {
        event.preventDefault();

        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <h1>Login</h1>
                {this.props.error &&  (
                    <Alert color="danger">{this.props.error.error || this.props.error.global}</Alert>
                )}
                <FormElement
                    propertyName="username"
                    title="Name"
                    value={this.state.username}
                    type="text"
                    onChange={this.inputChangeHandler}
                />
                <FormElement
                    propertyName="password"
                    title="Password"
                    type="password"
                    value={this.state.password}
                    onChange={this.inputChangeHandler}
                />
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">
                            Login
                        </Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Login);