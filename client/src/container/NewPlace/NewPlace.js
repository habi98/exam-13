import React, {Component} from 'react';
import {Button, Col, CustomInput, Form, FormGroup} from "reactstrap";
import FormElement from "../../component/UI/Form/FormElement";
import {createPlace} from "../../store/actions/placesActions";
import {connect} from "react-redux";

class NewPlace extends Component {
    state = {
        title: '',
        mainImage: '',
        description: '',
        conditions: "false"
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };



    inputChangeCheckboxHandler = event => {
        let conditions = this.state.conditions;

        if (event.target.checked) {
           conditions = event.target.value
        } else {
           conditions = null
        }

        this.setState({ conditions: conditions });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });


        this.props.createPlace(formData)
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <h2>Add new Place</h2>
                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                />

                <FormElement
                    propertyName="mainImage"
                    title="Image"
                    type="file"
                    onChange={this.fileChangeHandler}
                />
                <FormGroup>
                    <Col sm={{offset: 2, size: 10}}>
                        <div>
                            <CustomInput type="checkbox" onChange={this.inputChangeCheckboxHandler} value="true" id="exampleCustomCheckbox" label="Я принимаю условия" />
                        </div>
                    </Col>
                </FormGroup>
                <FormElement
                    propertyName="description"
                    title="Description"
                    type="textarea"
                    value={this.state.description}
                    onChange={this.inputChangeHandler}

                />

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="success">
                            Create
                        </Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createPlace: placeData => dispatch(createPlace(placeData))
});

export default connect(null, mapDispatchToProps)(NewPlace);