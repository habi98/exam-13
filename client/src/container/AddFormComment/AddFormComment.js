import React, {Component} from 'react';
import FormElement from "../../component/UI/Form/FormElement";
import Form from "reactstrap/es/Form";
import {Button, Col, Input, Label, Row} from "reactstrap";
import {createComment} from "../../store/actions/commentsAction";
import {connect} from "react-redux";

const rankNumber = [
    '5.0',
    '4.0', '4.1', '4.2', '4.3', '4.4', '4.5', '4.6','4.7','4.8', '4.9',
    '3.0', '3.1', '3.2', '3.3', '3.4', '3.5', '3.6','3.7','3.8', '3.9',
    '2.0', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6','2.7','2.8', '2.9',
    '1.0', '1.1', '1.2', '1.3', '1.4', '1.5', '1.6','1.7','1.8', '1.9',
];

class AddFormComment extends Component {
    state = {
        commentText: '',
        ratingQualityOfFood: '',
        ratingServiceQuality: '',
        ratingInterior: '',
        placeId: this.props.placeId
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.createComment({...this.state})

    };
    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <hr/>
                <FormElement
                    type="textarea"
                    propertyName="commentText"
                    title="Add a comment:"
                    onChange={this.inputChangeHandler}
                />

                <Row>
                    <Col sm={3}>
                        <Label>Easy to make </Label>
                        <Input
                            type="select"
                            name="ratingQualityOfFood"
                            value={this.state.ratingQualityOfFood}
                            onChange={this.inputChangeHandler}

                        >
                            {rankNumber.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3}>
                        <Label>Quick to make</Label>
                        <Input
                            type="select"
                            name="ratingServiceQuality"
                            value={this.state.ratingServiceQuality}
                            onChange={this.inputChangeHandler}
                        >
                            {rankNumber.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3}>
                        <Label>Taste</Label>

                        <Input
                            type="select"
                            name="ratingInterior"
                            onChange={this.inputChangeHandler}
                            value={this.state.ratingInterior}

                        >
                            {rankNumber.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3} className="p-4 mt-1">
                        <Button type="submit" color="primary">
                           Submit review
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createComment: commentData => dispatch(createComment(commentData))
})

export default connect(null, mapDispatchToProps)(AddFormComment);