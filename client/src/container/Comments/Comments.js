import React, {Component, Fragment} from 'react';
import {Card, CardText, CardTitle, Col, Row} from "reactstrap";
import StarRatings from 'react-star-ratings'
import {connect} from "react-redux";
import {fetchComments} from "../../store/actions/commentsAction";
import AverageRatings from "../../component/UI/AverageRatings/AverageRatings";

class Comments extends Component {


    componentDidMount() {
        this.props.fetchComments(this.props.placeId)
    }

    render() {

        let numOverall = 0;
        let numQualityOfFood = 0;
        let numServiceQuality = 0;
        let numInterior = 0;

        {this.props.comments.map(comment => {
            numQualityOfFood += comment.ratingQualityOfFood / this.props.comments.length;
            numServiceQuality += comment.ratingServiceQuality / this.props.comments.length;
            numInterior += comment.ratingInterior / this.props.comments.length;
            numOverall = (numQualityOfFood + numServiceQuality + numInterior) / 3
        })}
        return (
          <Fragment>
              <AverageRatings
                  numOverall={parseFloat(numOverall.toFixed(1))}
                  numQualityOfFood={parseFloat(numQualityOfFood.toFixed(1))}
                  numServiceQuality={parseFloat(numServiceQuality.toFixed(1))}
                  numInterior={parseFloat(numInterior.toFixed(1))}
              />

              {this.props.comments.length > 0 ? (
                  <div style={{height: '400px', overflow: 'auto'}}>
                      {this.props.comments.map(comment => (
                          <Card key={comment._id} className="mt-2 p-3">
                              <CardTitle>
                                  On {comment.dateTime}: <span>{comment.user.username}</span>
                              </CardTitle>
                              <CardText>Text: {comment.commentText}</CardText>


                              {comment.ratingQualityOfFood ? (


                                  <Row>
                                      <Col sm={4}>
                                          Quality Of Food:
                                      </Col>

                                      <Col sm={5}>
                                          <StarRatings
                                              rating={comment.ratingQualityOfFood}
                                              ratingToShow={comment.ratingQualityOfFood}
                                              starDimension={'25px'}
                                              starRatedColor="orange"
                                              numberOfStars={5}
                                          />


                                      </Col>
                                      <Col sm={2}>
                                      <span >
                                          {comment.ratingQualityOfFood}
                                       </span>
                                      </Col>
                                      <Col sm={4}>
                                          Service Quality:
                                      </Col>

                                      <Col sm={5}>
                                          <StarRatings
                                              rating={comment.ratingServiceQuality}
                                              ratingToShow={comment.ratingServiceQuality}
                                              starDimension={'25px'}
                                              starRatedColor="orange"
                                              numberOfStars={5}
                                          />
                                      </Col>
                                      <Col>
                                      <span>
                                       {comment.ratingServiceQuality}
                                     </span>
                                      </Col>
                                      <Col sm={4}>
                                          Interior:
                                      </Col>
                                      <Col sm={5}>
                                          <StarRatings
                                              rating={comment.ratingInterior}
                                              ratingToShow={comment.ratingInterior}
                                              starDimension={'25px'}
                                              starRatedColor="orange"
                                              numberOfStars={5}
                                          />
                                      </Col>
                                      <Col>
                                        <span>
                                         {comment.ratingInterior}
                                      </span>
                                      </Col>
                                  </Row>
                              ): null}

                          </Card>
                      ))}
                  </div>
              ): (
                  <div>
                      <hr/>
                     <h3>No Comments</h3>
                  </div>
              )}

          </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    comments: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
    fetchComments: placeId => dispatch(fetchComments(placeId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);