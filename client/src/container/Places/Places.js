import React, {Component, Fragment} from 'react';
import {Button, Card, CardColumns, CardImg, CardText, Col, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import config from '../../config'
import {connect} from "react-redux";
import {deleteOnePlace, fetchPlaces} from "../../store/actions/placesActions";

class Places extends Component {
    componentDidMount() {
        this.props.fetchPlaces();
    }

    render() {
        return (
            <Fragment>
                <CardColumns>
                    {this.props.places.map(place => (
                            <Card key={place._id}>
                                <NavLink tag={RouterNavLink} to={`/place/${place._id}`}>
                                    <CardImg top width="100%"  src={config.apiUrl + '/uploads/' + place.mainImage} alt="recipe" />
                                    <CardText style={{textAlign: 'center', padding: '10px 0'}}>{place.title}</CardText>
                                </NavLink>
                                {this.props.user && this.props.user.user.role === 'admin' ? (
                                    <Col sm={{offset: 0, size: 4}} className="p-3">
                                        <Button onClick={() => this.props.deleteOnePlace(place._id)} color="danger">Удалить</Button>
                                    </Col>
                                ): null}

                            </Card>
                    ))}
                </CardColumns>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    places: state.places.places,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
   fetchPlaces: () => dispatch(fetchPlaces()),
    deleteOnePlace: (placeId) => dispatch(deleteOnePlace(placeId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Places);