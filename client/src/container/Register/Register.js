import React, {Component} from 'react';
import FormElement from "../../component/UI/Form/FormElement";
import Form from "reactstrap/es/Form";
import {Alert, Button, Col, FormGroup} from "reactstrap";

import {connect} from "react-redux";
import {registerUser} from "../../store/actions/userActions";

class Register extends Component {

    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = (event) => {
        event.preventDefault();

        this.props.registerUser({...this.state})
    };


    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };


    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <h1>Register</h1>
                {this.props.error && !this.props.error.errors &&  (
                    <Alert color="danger">{this.props.error.error || this.props.error.global}</Alert>
                )}
                <FormElement
                    propertyName="username"
                    title="Name"
                    value={this.state.name}
                    type="text"
                    onChange={this.inputChangeHandler}
                    error={this.getFieldError('username')}

                />
                <FormElement
                    propertyName="password"
                    title="Password"
                    type="password"
                    value={this.state.password}
                    onChange={this.inputChangeHandler}
                    error={this.getFieldError('password')}

                />
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="success">
                            Register
                        </Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    registerError: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);