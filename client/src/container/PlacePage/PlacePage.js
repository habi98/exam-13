import React, {Component, Fragment} from 'react';
import {fetchOnePlace} from "../../store/actions/placesActions";
import {connect} from "react-redux";
import {Card, CardImg, CardText, Col, Row} from "reactstrap";
import config from '../../config'
import NewImage from "../NewImage/NewImage";
import CarouselGallery from "../../component/UI/CarouselGallery/CarouselGallery";
import AddFormComment from "../AddFormComment/AddFormComment";
import Comments from "../Comments/Comments";
class PlacePage extends Component {

    componentDidMount() {
        this.props.fetchOnePlace(this.props.match.params.id)
    }

    render() {
        if (!this.props.place) return null ;
        return (
            <Fragment>
                <Card className="p-2">
                    <Row>
                        <Col sm={8}>
                            <h2>{this.props.place.title}</h2>
                            <CardText>
                                {this.props.place.description}
                            </CardText>
                        </Col>
                        <Col sm={4}>
                            <CardImg src={config.apiUrl + '/uploads/' +  this.props.place.mainImage} alt="recipe"/>
                        </Col>
                    </Row>
                </Card>
                {this.props.place.gallery.length > 0 ? (
                    <Fragment>
                        <hr/>
                        <h3>Images</h3>
                        <CarouselGallery place={this.props.place}/>
                    </Fragment>
                ): (
                    <div>
                        <hr/>
                        <h3>No Images</h3>
                    </div>
                )}

                <Comments placeId={this.props.match.params.id}/>

               {this.props.user && this.props.user.user._id !== this.props.place.user &&  <AddFormComment placeId={this.props.match.params.id}/>}

               {this.props.user && <NewImage placeId={this.props.match.params.id}/>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    place: state.places.place
});

const mapDispatchToProps = dispatch => ({
    fetchOnePlace: placesId => dispatch(fetchOnePlace(placesId))
});

export default connect(mapStateToProps, mapDispatchToProps)(PlacePage);